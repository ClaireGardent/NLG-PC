tercom.pl README
Matthew Snover
Version 6b - 4/10/2006

=========================================================

CopyrightŠ 2005-2006 by BBN Technologies and University of Maryland (UMD)

BBN and UMD grant a nonexclusive, source code, royalty-free right to
use this Software known as Translation Error Rate COMpute (the
"Software") solely for research purposes. Provided, you must agree
to abide by the license and terms stated herein. Title to the
Software and its documentation and all applicable copyrights, trade
secrets, patents and other intellectual rights in it are and remain
with BBN and UMD and shall not be used, revealed, disclosed in
marketing or advertisement or any other activity not explicitly
permitted in writing.

BBN and UMD make no representation about suitability of this
Software for any purposes.  It is provided "AS IS" without express
or implied warranties including (but not limited to) all implied
warranties of merchantability or fitness for a particular purpose.
In no event shall BBN or UMD be liable for any special, indirect or
consequential damages whatsoever resulting from loss of use, data or
profits, whether in an action of contract, negligence or other
tortuous action, arising out of or in connection with the use or
performance of this Software.

Without limitation of the foregoing, user agrees to commit no act
which, directly or indirectly, would violate any U.S. law,
regulation, or treaty, or any other international treaty or
agreement to which the United States adheres or with which the
United States complies, relating to the export or re-export of any
commodities, software, or technical data.  This Software is licensed
to you on the condition that upon completion you will cease to use
the Software and, on request of BBN and UMD, will destroy copies of
the Software in your possession.

=========================================================

The program to calculate the TER (Translation Error Rate) score is
tercom_v6b.pl.  (Formerly known as calc_ter)

RECENT CHANGES
----------------------------------------------

Version 6b:
  Added document level scoring (.sum_doc output)
  Added support for n-best list scoring (.sum_nbest output)
  Added option -N to normalize hyp and ref as in MT eval scoring
  Increased precision of floating point output

Version 5:
  Changed program name from calc_ter to tercom
  Changed default options (-f 2 is now default)
  Added support for sgm files (-i sgm)
  Added option to specify beam width

SAMPLE FILES
----------------------------------------------

The sample_data directory contains 100 sentences from reference and
hypothesis data from MTEval 2004.  All references were created by NIST
(none are targeted references).

sample_data/hyp.txt contains the hypothesis data.
sample_data/ref.txt contains the reference data (4 references/sentence).

Running
tercom_v6b.pl -N -r sample_data/ref.txt -h sample_data/hyp.txt
will generate the results found in
sample_data/hyp.txt.sys.sum.orig and sample_data/hyp.txt.sys.pra.orig

(SGM files can be used by using the "-i sgm" flag on tercom.pl)

USAGE
----------------------------------------------

The recommended usage of the script is:
tercom.pl -r <reffile> -h <hypfile> -s -N

If running with human targeted references, usage should be:
tercom.pl -r <targ-reffile> -h <hypfile> -a <untarg-reffile> -s -N

If you want to use sgml files, add in the "-i sgm" flag.

INPUTS
----------------------------------------------

There are two required arguments, -r and -h, which specify the
reference and hypothesis files respectively.  Both files must contain
one sentence/segment per line (blank lines and lines beginning with #
are ignored).  Each sentence must have an id tag at the end,
surrounded in parentheses, for example:(AFA20040101.0510-8) .
Multiple references for a sentence should all have the same id tag.  I
recommend that the id tags be (DOCID-SEGNUM).

This version of perl, supports sgml files, such as those used by BLEU.
If using sgml files, the "-i sgm" flag should be given.

The denominator for calculating TER is the average length of the
reference sentences.  If you have a human reference in your reference
file, or want to use some other sentences to compute the denominator,
use the -a flag to specify a secondary reference file.

By default, tercom.pl is not case sensitive.  Use the -s flag to
turn on case sensitivity.

There are currently four output file types supported, sum, pra,
sum_doc, and sum_nbest.  By default all files are generated.  You can
specify that you only want one or the other using the -o flag, with
the types separated with commas (pra,sum for example).  The sum_doc
file contains document level scores, and the sum_nbest file contains
nbest summary output, given nbest input.

The default tokenization used by the script is very poor.  It is
recommended that, if you want the code to tokenize, that the -N flag
be used.  This will enable MTEval style tokenization.

If you fail to specify the -r and -h flags, the code will show a usage
statement with the other arguments.

OUTPUTS
----------------------------------------------------

By default all output files are written to <hyp-file>.sys.<out-type>.
(For example, hyp.sys.pra or hyp.sys.sum).  A name other than
<hyp-file> can be specified using the -n flag.

There are two output files generated by calc_ter.pl by default:

sum (summary file) - This file contains the TER score for each
sentence, along with the total TER score for the dataset.  A
breakdown into insertions, deletions, substitutions, and shifts is
also provided.  The total number of words shifted is also provided. 

pra (alignment file) - This file contains the alignments generated by
the TER for each segment.  The format of the output is very similar to
that of sclite. By default, detailed shifted information is also
provided (although this can be disabled using the -S flag).

sum_doc (document summary file) - This file contains the TER score for
each document.  A breakdown into insertions, deletions, substitutions,
and shifts is also provided.  The total number of words shifted is
also provided.  The score for a document is the sum of the number of
edits in all of the segments in the document, divided by the number of
words in the entire document.

sum_nbest (nbest summary file) - This file contains summary scores for
use with nbest inputs.

Only scores and alignments for the closest reference (the one with the
lowest TER score) are output.

CITATION
----------------------------------------------------
Snover, Matthew, Bonnie J. Dorr, Richard Schwartz, John Makhoul, Linnea
Micciulla, Ralph Weischedel, "A Study of Translation Error Rate with
Targeted Human Annotation," LAMP-TR-126, CS-TR-4755,
UMIACS-TR-2005-58, University of Maryland, College Park, MD July,
2005.
