# NLG-PC : Text Generation Task 

## Introduction

Ajinkya Kulkarni, Guillaume Le Berre 




# The delexicalisation and relexicalisation process

Go to Code/ directory 

## Delexicalisation

To run the delexicalization, write:

```bash
python ./parseXML.py -i <inputdir> --mode <mode>
```

- <inputdir> is the path to the folder containing the subfolders 'train' and 'dev'.
- <mode> is a number contained in [1,2].
    - '1' compute the delexicalisation and include all semantic types for a given subject or object.
    - '2' compute the delexicalisation and select only one semantic type for each subject and object.

This command will preprocessed the data and create three types of file (one triple for each set of data):

- .triple for the input triples.
- .lex for the target sentences.
- .rplc for the replacement dictionary.

## Relexicalisation

To relexicalise a file, write:

```bash
python ./relex.py --rplcdict <rplcdict> --delexfile <delexfile> --relexfile <relexfile>
```

- <rplcdict> the replacement dictionary.
- <delexfile> the file to relexicalise.
- <relexfile> (optional) the path and name of the file where the relexicalisation will be saved. Default is <delexfile>_relex.

# Convolutional Sequnece to Sequence Learning

git clone https://github.com/tobyyouup/conv_seq2seq.git

Requirement

    Python 2.7.0+
    Tensorflow 1.0+ (this version is strictly required)
    and their dependencies

Install 

Run the command given below, 

sudo apt-get install google-perftools
export LD_PRELOAD="/usr/lib/libtcmalloc.so.4" 

set LD_PRELOAD

Go to CNN_seq2seq_model_file and unzip the model directoris

## Training : 

Go to /conv_seq2seq-master/train_conv_seq2seq.sh and edit the following paths as 
export VOCAB_SOURCE=${DATA_PATH}/vocab.triple
export VOCAB_TARGET=${DATA_PATH}/vocab.lex
export TRAIN_SOURCES=${DATA_PATH}/train_v2.triple
export TRAIN_TARGETS=${DATA_PATH}/train_v2.lex
export DEV_SOURCES=${DATA_PATH}/dev_v2.triple
export DEV_TARGETS=${DATA_PATH}/dev_v2.lex
export TEST_SOURCES=${DATA_PATH}/test_v2.triple
export TEST_TARGETS=${DATA_PATH}/test_v2.lex
 and run the 

./train_conv_seq2seq.sh

After the traing, Dont forget to copy the model file from tmp folder

## Testing

Update the following paths in test_conv_seq2seq_text_summerisation.sh

export LD_PRELOAD="/usr/lib/libtcmalloc.so.4"
set LD_PRELOAD
export MODEL_DIR=$path_to_train_model_directory
export TEST_SOURCES=${DATA_PATH}/test_v2.triple
export PRED_DIR=${MODEL_DIR}/pred
mkdir -p ${PRED_DIR}

Depending on the model you want to run with the decoder either beam search or greedy search edit the test_conv_seq2seq_text_summerisation.sh file and run the script

./test_conv_seq2seq_text_summerisation.sh

Check the output in the model_file_directory in the ${Model_File_Path}/pred/pred.txt 

# Preprocess, Train and Translate the OpenNMT models.

Requirements : 

Install OpenNMT-py

Go to OpenNMT-py directory,

Step 1. Preprocess the data : 

python preprocess.py -train_src $train_path_of_triple_file -train_tgt $train_path_of_lex_file -valid_src $valid_path_of_triple_file -valid_tgt valid_path_of_lex_file -save_data $Name_of_data_path

Step 2. Train the data 

python train.py -data $data_file_path -save_model  $model_file_path

Step 3. Translate the data (Test the data)

python translate.py -model $path_to_model_file -src $test_file_triple_path -output $output_lex_file_path  -replace_unk

# TO evaluate the TER, BLEU and Metoer scores :

## TER and Metoer Score : 
Go to Final_Evaluation_results/Final_Evaluation_TER_METEOR/METEOR_TER_Scores/ directory and run 

python metrics.py --ref $groundtruth_relexfilepath --out $Output_relexfilepath

Go to tercom_v6b directory 

here reffile and hypfiles are created by above python scripts all-notdelex-refs-ter.txt, and relexicalised_predictions-ter.txt repectively

tercom.pl -r <reffile> -h <hypfile> -s -N

Similarly go to Final_Evaluation_results/Final_Evaluation_TER_METEOR/METEOR_TER_Scores/meteor-1.5 directory

java -Xmx2G -jar meteor-1.5.jar <data-directory>/relexicalised_predictions.txt <data-directory>/all-notdelex-refs-meteor.txt -l en -norm -r 8 >metoer_eval_log.txt

## BLEU score 
Go to Final_Evaluation_results/Final_Evaluation_TER_METEOR/BLEU_Scores directory

Run ./multi-bleu.perl $reference_file < $hypothesis_File >BLEU_score_output.txt


# To analysise the Evaluation Results, please refer the following directory structure, 

1. BLEU Scores

Go to /BLEU_score/ directory

2. TER Scores

Go to /TER_Scores/ directory :

/cnn/v1/beam/relexicalised_predictions-ter.txt.sys.sum_doc
cnn/v1/greedy/relexicalised_predictions-ter.txt.sys.sum_doc
cnn/v2/beam/relexicalised_predictions-ter.txt.sys.sum_doc
cnn/v2/greedy/relexicalised_predictions-ter.txt.sys.sum_doc

test/v1/1/relexicalised_predictions-ter.txt.sys.sum_doc
test/v1/2/relexicalised_predictions-ter.txt.sys.sum_doc
test/v1/3/relexicalised_predictions-ter.txt.sys.sum_doc
test/v1/4/relexicalised_predictions-ter.txt.sys.sum_doc
test/v2/1/relexicalised_predictions-ter.txt.sys.sum_doc
test/v2/2/relexicalised_predictions-ter.txt.sys.sum_doc
test/v2/3/relexicalised_predictions-ter.txt.sys.sum_doc
test/v2/4/relexicalised_predictions-ter.txt.sys.sum_doc

3. Meteor Scores

Go to /Meteor_Scores/
