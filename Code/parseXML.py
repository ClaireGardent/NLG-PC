import os
import random
import re
import json
import sys
import getopt
from collections import defaultdict
from benchmark_reader import Benchmark
from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setReturnFormat(JSON)
query = """
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    SELECT ?type
    WHERE { <http://dbpedia.org/resource/%s> rdf:type ?type .
    FILTER regex(?type, "ontology/") }
""" # template for dbpedia queries


def select_files(topdir, size=(1, 8)):
    """
    Collect all xml files from a benchmark directory.
    :param topdir: directory with benchmark - import data into folder with benchmark
    :param category: specify DBPedia category to retrieve texts for a specific category (default: retrieve all)
    :param size: specify size to retrieve texts of specific size (default: retrieve all)
    :return: list of tuples (full path, filename)
    """
    finaldirs = [topdir+'/'+str(item)+'triples' for item in range(size[0], size[1])] #path for each item in folder
    finalfiles = []
    for item in finaldirs: #creating a list of tuples, first
        finalfiles += [(item, filename) for filename in os.listdir(item)] #listdir like ls in bash, item is the path
    return finalfiles


def delexicalisation(out_src, out_trg, category, properties_objects):
    """
    --Outdated
    Perform delexicalisation.
    :param out_src: source string
    :param out_trg: target string
    :param category: DBPedia category
    :param properties_objects: dictionary mapping properties to objects
    :return: delexicalised strings of the source and target; dictionary containing mappings of the replacements made
    """
    with open('delex_dict.json', encoding='utf8') as data_file:
        data = json.load(data_file) #categories for subjects
    # replace all occurrences of Alan_Bean to ASTRONAUT in input
    delex_subj = data[category] #subject under category in json
    delex_src = out_src #source triples
    delex_trg = out_trg # target lexs
    # for each instance, we save the mappings between nondelex and delex
    replcments = {}
    for subject in delex_subj: #for subject under each category
        clean_subj = ' '.join(re.split('(\W)', subject.replace('_', ' '))) # split punctuation, repl.underscore
        if clean_subj in out_src: #for subject in source triple
            delex_src = out_src.replace(clean_subj + ' ', category.upper() + ' ') #subject replaced by category in CAPITAL LETTERS and a space
            replcments[category.upper()] = ' '.join(clean_subj.split())   # remove redundant spaces, keep mapping bw the word and a category
        if clean_subj in out_trg: #for subject in the lexs sentence
            delex_trg = out_trg.replace(clean_subj + ' ', category.upper() + ' ') #replace by category
            replcments[category.upper()] = ' '.join(clean_subj.split()) #keep mapping

    # replace all occurrences of objects by PROPERTY in input
    for pro, obj in sorted(properties_objects.items()): #dictionary properties, objects
        obj_clean = ' '.join(re.split('(\W)', obj.replace('_', ' ').replace('"', ''))) #no punctuation
        if obj_clean in delex_src: #for source triple
            delex_src = delex_src.replace(obj_clean + ' ', pro.upper() + ' ') #property in upper case
            replcments[pro.upper()] = ' '.join(obj_clean.split())   # remove redundant spaces
        if obj_clean in delex_trg: #for lex
            delex_trg = delex_trg.replace(obj_clean + ' ', pro.upper() + ' ')
            replcments[pro.upper()] = ' '.join(obj_clean.split())

    # possible enhancement for delexicalisation:
    # do delex triple by triple  - for sub, pro, ob
    # now building | location | New_York_City New_York_City | isPartOf | New_York
    # is converted to
    # BUILDING location ISPARTOF City ISPARTOF City isPartOf ISPARTOF
    return delex_src, delex_trg, replcments


def get_type(str, default, mode):
    """
    return the semantic type of the given str using the dbpedia API
    :param str: a string containing a subject or an object
    :param default: the default type if dbpedia is unable to give a type for the entry
    :return:
    """
    semantic_types = ['ACTIVITY', 'ORGANISATION', 'PERSON', 'AGENT', 'AREA', 'COLOUR', 'CURRENCY', 'DEVICE', 'DISEASE', 'EVENT', 'FLAG', 'FOOD', 'LANGUAGE', 'AIRCRAFT', 'AUTOMOBILE', 'SHIP', 'MEANOFTRANSPORTATION', 'MEDIA', 'NAME', 'ARCHITECTURALSTRUCTURE', 'NATURALPLACE', 'COUNTRY', 'REGION', 'CITY', 'PLACE', 'ANIMAL', 'PLANT', 'SPECIES', 'FILM', 'WRITTENWORK', 'MUSICALWORK', 'CARTOON', 'ARTWORK', 'WORK']
    try:
        sparql.setQuery(query % str)
        results = sparql.query().convert()
        types = [results["results"]["bindings"][k]["type"]["value"].rsplit('/', 1)[-1].upper() for k in range(len(results["results"]["bindings"]))]
    except:
        return default

    if mode == 1:
        types.sort()
        return " ".join(types) if types != [] else default
    elif mode == 2:
        min_index = len(semantic_types)
        for t in types:
            if t in semantic_types:
                min_index = min(semantic_types.index(t), min_index)
        return semantic_types[min_index] if min_index != len(semantic_types) else default
    else:
        return default


def delex_triples(raw_triples, mode):
    """
    delexicalize the triples and compute the replacement dictionaries
    :param raw_triples:
    :return:
    """
    index_last_entity = 0
    rplc_dict = dict()
    rplc_inv_dict = dict()
    triples = ''
    for triple in raw_triples:
        triple.p = camel_case_tokenize(triple.p)
        raw_subject = triple.s.split(",_")
        subject = []
        for s in raw_subject:
            type = get_type(s.replace(' ', ''), "DEFAULTTYPE", mode)
            if s in rplc_inv_dict:
                subject.append(rplc_inv_dict[s])
            else:
                index_last_entity += 1
                id = "ENTITY" + str(index_last_entity)
                rplc_inv_dict[s] = id
                rplc_dict[id] = s
                subject.append(id)
            subject.append(type)
        triples += ' '.join(subject) + ' '
        object = triple.o.replace('"', '').replace("'", '')
        type = get_type(object, triple.p.replace(' ', '').upper(), mode)
        if object in rplc_inv_dict:
            id = rplc_inv_dict[object]
        else:
            index_last_entity += 1
            id = "ENTITY" + str(index_last_entity)
            rplc_inv_dict[object] = id
            rplc_dict[id] = object
        triples += triple.p + ' ' + id + ' ' + type + ' '
    return rplc_dict, rplc_inv_dict, triples


def delex_lex(raw_lex, rplc_inv_dict):
    raw_lex = raw_lex.replace("'", '')
    for key in rplc_inv_dict:
        try:
            val = float(key)
            sentence = re.split('(-?[0-9][0-9,.]*)', raw_lex)
            if sentence[-1] == '':
                sentence[-2] = sentence[-2][:-1]
            for i in range(len(sentence)):
                if sentence[i] != '' and sentence[i][0] in ['-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] and val == float(sentence[i].replace(',', '')):
                    sentence[i] = ' ' + rplc_inv_dict[key] + ' '
            raw_lex = ''.join(sentence)
        except:
            if key.replace('_', ' ').lower() in raw_lex:
                raw_lex = raw_lex.replace(key.replace('_', ' ').lower(), ' ' + rplc_inv_dict[key] + ' ')
    return raw_lex


def create_source_target(b, dataset, mode):
    """
    Write target and source files, and reference files for BLEU.
    :param b: instance of Benchmark class
    :param options: string "delex" or "notdelex" to label files
    :param dataset: dataset part: train, dev, test
    :param delex: boolean; perform delexicalisation or not
    :return: if delex True, return list of replacement dictionaries for each example
    """
    source_out = [] #source delexicalized triples
    target_out = [] #target sentences
    rplc_list = []  # store the dict of replacements for each example
    n = len(b.entries)
    lastper = -1
    for i in range(n): #parsing of the XML
        if lastper != int((i/n)*100):
            print("%s %d%%" % (dataset, int((i/n)*100)))
            lastper = int((i/n)*100)
        entr = b.entries[i]
        tripleset = entr.modifiedtripleset # Aarhus_Airport | city | Aarhus, Denmark
        lexics = entr.lexs # sentence lexicalized
        category = entr.category #Airport
        (rplc_dict, rplc_inv_dict, triples) = delex_triples(tripleset.triples, mode)
        out_src = ' '.join(re.split('\W', triples)) # commas separated from the strings, triples = out_src
        for lex in lexics: #for each sentence
            rplc_list.append(rplc_dict)
            source_out.append(' '.join(out_src.split()))
            modified_lex = delex_lex(lex.lex.lower(), rplc_inv_dict)

            # separate punct signs from text
            out_trg = ' '.join(re.split('\W', modified_lex)) # split into list on non-alphabetic symbol (,.!), join by space

            # delete white spaces
            target_out.append(' '.join(out_trg.split()))

    # shuffle two lists in the same way    ##why? for evaluation??
    random.seed(10)
    corpus = list(zip(source_out, target_out, rplc_list))
    random.shuffle(corpus)
    source_out, target_out, rplc_list = zip(*corpus)

    with open(dataset + '_v' + str(mode) + '.triple', 'w+', encoding="utf8") as f:
        f.write('\n'.join(source_out))
        f.close()
    with open(dataset + '_v' + str(mode) + '.lex', 'w+', encoding="utf8") as f:
        f.write('\n'.join(target_out))
        f.close()
    with open(dataset + '_v' + str(mode) + '.rplc', 'w+', encoding="utf8") as f:
        json.dump(rplc_list, f, indent=2)
        f.close()


def camel_case_tokenize(triple_property):
    """
    tokenize the property
    :param triple_property:
    :return:
    """
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', triple_property)
    tokens = [m.group(0).lower() for m in matches]
    clear_tokens = ' '.join(tokens).replace('(', '').replace(')', '').replace('_', ' ')
    return clear_tokens


def input_files(path, mode): #input files for implementing the baseline, running for results
    """
    Read the corpus, write train and dev files. #training and development, after delex and tokenization
    :param path: directory with the WebNLG benchmark
    :param filepath: path to the prediction file with sentences (for relexicalisation)
    :param relex: boolean; do relexicalisation or not
    :return:
    """
    parts = ['train', 'dev', 'test']
    for part in parts:
        files = select_files(path + part, size=(1, 6)) #path of the top directory, choose either train or dev
        b = Benchmark()
        b.fill_benchmark(files) # upload files through benchmark
        create_source_target(b, part, mode) #list of preprocessed


def main(argv):
    usage = 'usage:\npython ./parseXML.py -i <data-directory> optional(--mode <mode>)'\
            '\ndata-directory is the directory where you unzipped the archive with data'\
            '\nmode can be 1 or 2 depending on the way semantic types will be processed'
    try:
        opts, args = getopt.getopt(argv, 'i:', ['inputdir=', 'mode='])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    input_data = False
    mode = 1
    for opt, arg in opts:
        if opt in ('-i', '--inputdir'):
            inputdir = arg
            input_data = True
        elif opt == "--mode":
            mode = int(arg)
        else:
            print(usage)
            sys.exit()
    if (not input_data) or (mode not in [1, 2]):
        print(usage)
        sys.exit(2)
    print('Input directory is ', inputdir)
    input_files(inputdir, mode)


if __name__ == "__main__":  #for running in command line
    main(sys.argv[1:])
