import os
import random
import re
import json
import sys
import getopt
from collections import defaultdict

def relexicalise(rplcfile, delexfile, relexfile):
    """
    Take a file from seq2seq output and write a relexicalised version of it.
    :param rplc_list: list of dictionaries of replacements for each example (UPPER:not delex item)
    :return: list of predicted sentences
    """
    relex_predictions = []

    with open(rplcfile, 'r') as f:
        rplc_list=json.load(f)

    with open(delexfile, 'r') as f:
        predictions = [line for line in f]
    for i, pred in enumerate(predictions):
        # replace each item in the corresponding example
        rplc_dict = rplc_list[i]
        relex_pred = pred
        for key in sorted(rplc_dict):
            relex_pred = relex_pred.replace(key, rplc_dict[key])
        relex_predictions.append(relex_pred)
    if relexfile:
        outputfile_name = relexfile
    else:
        temp = delexfile.split('.')
        if len(temp) >= 2:
            temp[-2] += '_relex'
        else:
            temp[-1] += '_relex'
        outputfile_name = '.'.join(temp)
    with open(outputfile_name, 'w') as f:
        f.write(''.join(relex_predictions))



def main(argv):
    usage = 'usage:\npython3 relex.py --rplcfile <rplcfile> --delexfile <delexfile> --relexfile <relexfile>' \
            '\nrplcfile is the replacement dictionary and delexfile is the file you want to relexicalise' \
            '\nrelexfile is optional and default name for relexicalized file is <delexfile>_relex'
    try:
        opts, args = getopt.getopt(argv, 'i:', ['rplcfile=', 'delexfile=', 'relexfile='])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    rplcdict = False
    inputfile = False
    relexfile = None
    for opt, arg in opts:
        if opt in ('-i', '--rplcfile'):
            rplcfile = arg
            rplcdict = True
        elif opt == '--delexfile':
            delexfile = arg
            inputfile = True
        elif opt == '--relexfile':
            relexfile = arg
        else:
            print(usage)
            sys.exit()
    if not (inputfile and rplcdict):
        print(usage)
        sys.exit(2)
    print('File to relexicalise is %s with dict %s' % (delexfile, rplcfile))
    relexicalise(rplcfile, delexfile, relexfile)

if __name__ == "__main__":  #for running in command line
    main(sys.argv[1:])
