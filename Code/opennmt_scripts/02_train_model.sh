#!/bin/bash -e

if test "$#" -ne 2; then
    echo "################################"
    echo "Usage: "
    echo "./02_train_model.sh <path_to_data_file> <model_file_name>"
    echo "################################"
    exit 1
fi

python train.py -data $1 -save_model $2