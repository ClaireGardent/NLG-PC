#!/bin/bash -e

if test "$#" -ne 2; then
    echo "################################"
    echo "Usage: "
    echo "./03_translate_model.sh <path_to_model_file> <input_text_file> <output_text_file>"
    echo "################################"
    exit 1
fi

python translate.py -model $1 -src $2 -output $3 -replace_unk -verbose