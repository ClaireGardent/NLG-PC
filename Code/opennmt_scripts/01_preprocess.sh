#!/bin/bash -e

if test "$#" -ne 2; then
    echo "################################"
    echo "Usage: "
    echo "./01_preprocess.sh <path_to_data_file> <model_file_name>"
    echo 'Default path : data/Text_Summerisation'
    echo "################################"
    exit 1
fi

src_train=$1/train.triple
tgt_train=$1/train.lex
src_val=$1/dev.triple
tgt_val=$1/dev.lex

python preprocess.py -train_src ${src_train} -train_tgt ${tgt_train} -valid_src ${src_val} -valid_tgt ${tgt_val} -save_data $2
