# The delexicalisation and relexicalisation process

## Delexicalisation

To run the delexicalization, write:

```bash
python ./parseXML.py -i <inputdir> --mode <mode>
```

- <inputdir> is the path to the folder containing the subfolders 'train' and 'dev'.
- <mode> is a number contained in [1,2].
    - '1' compute the delexicalisation and include all semantic types for a given subject or object.
    - '2' compute the delexicalisation and select only one semantic type for each subject and object.

This command will preprocessed the data and create three types of file (one triple for each set of data):

- .triple for the input triples.
- .lex for the target sentences.
- .rplc for the replacement dictionary.

## Relexicalisation

To relexicalise a file, write:

```bash
python ./relex.py --rplcdict <rplcdict> --delexfile <delexfile> --relexfile <relexfile>
```

- <rplcdict> the replacement dictionary.
- <delexfile> the file to relexicalise.
- <relexfile> (optional) the path and name of the file where the relexicalisation will be saved. Default is <delexfile>_relex.